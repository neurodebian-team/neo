Source: neo
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Michael Hanke <mih@debian.org>,
           Yaroslav Halchenko <debian@onerussian.com>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: python3-all,
               debhelper-compat (= 13),
               dh-sequence-python3,
               ipython3,
               pybuild-plugin-pyproject,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-design,
               python3-sphinx-inline-tabs,
               python3-matplotlib,
               python3-nixio <!nocheck>,
               python3-numpy,
               python3-pickleshare,
               python3-pil,
               python3-pydata-sphinx-theme,
               python3-quantities (>= 0.16.1~),
               python3-pytest
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/neo
Vcs-Git: https://salsa.debian.org/med-team/neo.git
Homepage: http://neuralensemble.org/trac/neo
Rules-Requires-Root: no

Package: python3-neo
Architecture: all
Section: python
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-nixio,
         python3-numpy,
         python3-pil,
         python3-quantities (>= 0.16.1~)
Recommends: python3-scipy,
            python3-tables,
            libjs-jquery,
            libjs-underscore
Suggests: python3-pytest
Description: Python IO library for electrophysiological data formats
 NEO stands for Neural Ensemble Objects and is a project to provide common
 classes and concepts for dealing with electro-physiological (in vivo
 and/or simulated) data to facilitate collaborative software/algorithm
 development. In particular Neo provides: a set a classes for data
 representation with precise definitions, an IO module with a simple API,
 documentation, and a set of examples.
 .
 NEO offers support for reading data from numerous proprietary file formats
 (e.g. Spike2, Plexon, AlphaOmega, BlackRock, Axon), read/write support for
 various open formats (e.g. KlustaKwik, Elan, WinEdr, WinWcp, PyNN), as well
 as support common file formats, such as HDF5 with Neo-structured content
 (NeoHDF5, NeoMatlab).
 .
 Neo's IO facilities can be seen as a pure-Python and open-source Neuroshare
 replacement.
